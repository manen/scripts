# Scripts

A collection of scripts I use on my Arch Linux computer.

Note: Some scripts might require [Node.js](https://nodejs.org/). Make sure it's installed on your system.

## Install

```bash
curl https://gitlab.com/manen/scripts/-/raw/master/install.sh | bash
```

Call `updatemanenscripts` or this to update.

## Help

`updatemanenscripts`: Calls install.sh
`codea`: Opens Visual Studio Code in current working directory, then closes the terminal emulator
`initreact <name>`: Calls create-react-app `<name>`, then opens Visual Studio Code and closes the terminal emulator.
`nodetestproj <name>`: Creates a boilerplate project called `<name>`
`nodemontestproj <name>`: Creates a boilerplate project with nodemon called `<name>`

## License

[GPL 3.0](LICENSE.txt)
