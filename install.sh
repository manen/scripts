#!/usr/bin/bash

REPO=https://gitlab.com/manen/scripts.git

MANENSCRIPTSHOME="$HOME/.manenscripts"
MANENSCRIPTSSCR="$MANENSCRIPTSHOME/scripts"

UPDATE=-1

update() {
    if [ -d $MANENSCRIPTSHOME ]
    then
        UPDATE=1

        echo "manen scripts installation found, updating"

        cd $HOME
        rm -rf $MANENSCRIPTSHOME
    else
        UPDATE=0
    fi
}

clone() {
    cd $HOME
    git clone $REPO $MANENSCRIPTSHOME
}

inject() {
    if [[ "$UPDATE" == 1 ]]
    then
        return
    fi

    TOADD="\n# manen scripts injection begins\nexport PATH=\$PATH:$MANENSCRIPTSSCR\nexport MANENSCRIPTSHOME=$MANENSCRIPTSHOME\nexport MANENSCRIPTSSCR=$MANENSCRIPTSSCR\n# manen scripts injection ends\n"

    if [ -f "$HOME/.zshrc" ]
    then
        echo -e $TOADD >> $HOME/.zshrc
    else if [ -f "$HOME/.bashrc" ]
        then
            echo -e $TOADD >> $HOME/.bashrc
        else
            echo -e $TOADD >> $HOME/.profile
        fi
    fi
}

chmodify() {
    find $MANENSCRIPTSSCR -type f -exec chmod +x {} \;
}

update
clone
inject
chmodify
